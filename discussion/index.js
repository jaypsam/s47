//alert ("Hello World")

console.log(document.querySelector("#txt-first-name"))
console.log(document)

// DOM Manipulation

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(spanFullName);

// Event

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value
// })

txtFirstName.addEventListener('keyup', printFirstName)

function printFirstName(event){
	spanFullName.innerHTML = txtFirstName
}

// Event listener

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
})

const labelFirstName = document.querySelector("#label-txt-name");

labelFirstName.addEventListener('click', (e) => {
	console.log(e)
	alert("You clicked the First Name Label.")
})