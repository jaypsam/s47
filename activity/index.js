const fullNameTxt = document.querySelector('#full-name');
const firstNameTxt = document.querySelector('#first-name');
const lastNameTxt = document.querySelector('#last-name');

let firstName = '';
let lastName = ''


function printFirstName(){
    firstName = firstNameTxt.value;
    fullNameTxt.innerHTML = `${firstName} ${lastName}`;
}

function printLastName(){
    lastName = lastNameTxt.value;
    fullNameTxt.innerHTML = `${firstName} ${lastName}`;
}

firstNameTxt.addEventListener('keyup', printFirstName);
lastNameTxt.addEventListener('keyup', printLastName);
